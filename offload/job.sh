#!/bin/bash

#SBATCH --job-name=cell
#SBATCH --time=00:20:00
#SBATCH --account=nn2849k
#SBATCH --mem-per-cpu=2G
#SBATCH --output=Output_File.out

##SBATCH --ntasks=4
##SBATCH --ntasks-per-node=4
#SBATCH --exclusive
##SBATCH --cpus-per-task=16

source /cluster/bin/jobsetup
cp $SUBMITDIR/cell $SCRATCH


#export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
time ./cell
